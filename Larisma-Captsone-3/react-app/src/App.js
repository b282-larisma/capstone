import AppNavbar from './components/AppNavbar';
import AppNavbarAdmin from './components/AppNavbarAdmin';
import ProductView from './components/ProductView';
import ProductViewAdmin from './components/ProductViewAdmin';
import './App.css';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products';
import Cart from './pages/Cart';
import CashIn from './pages/CashIn';
import Profile from './pages/Profile'; 
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminHome from './pages/AdminHome';
import AdminAllProducts from './pages/AdminAllProducts';
import AdminCreateProduct from './pages/AdminCreateProduct';
import AdminAllUsers from './pages/AdminAllUsers';
import AdminAllOrders from './pages/AdminAllOrders';

import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer  ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        {user.isAdmin ? <AppNavbarAdmin /> : <AppNavbar />}
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/cashin" element={<CashIn />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/*" element={<Error />} />
            {user.isAdmin && (
              <>
                <Route path="/admin-home" element={<AdminHome />} />
                <Route path="/admin-all-products" element={<AdminAllProducts />} />
                <Route path="/admin-all-users" element={<AdminAllUsers />} />
                <Route path="/admin-all-orders" element={<AdminAllOrders />} />
                <Route path="/admin-product/:productId" element={<ProductViewAdmin />} />
                <Route path="/admin-create-product" element={<AdminCreateProduct />} />
              </>
            )}
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
