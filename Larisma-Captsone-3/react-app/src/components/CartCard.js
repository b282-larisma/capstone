import React, { useState } from 'react';
import { Card, Row, Col, Button, Form } from 'react-bootstrap';

export default function CartCard({ item, onCheckout }) {
  const { product, total, _id: cartId } = item;
  const { productName, quantity } = product[0];
  const [modeOfPayment, setModeOfPayment] = useState('Cash on Delivery');

  const handlePaymentChange = (e) => {
    setModeOfPayment(e.target.value);
  };

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{productName}</h4>
            </Card.Title>
            <Card.Subtitle>Quantity</Card.Subtitle>
            <Card.Text>{quantity}</Card.Text>
            <Card.Subtitle>Total</Card.Subtitle>
            <Card.Text>PhP {total}</Card.Text>

            <Form.Group controlId="modeOfPayment">
              <Form.Label>Mode of Payment</Form.Label>
              <Form.Control as="select" value={modeOfPayment} onChange={handlePaymentChange}>
                <option>Cash on Delivery</option>
                <option>Credit Card</option>
                <option>Bank Transfer</option>
                <option>PayPal</option>
                <option>Gcash</option>
              </Form.Control>
            </Form.Group>

            <Button onClick={() => onCheckout(cartId, modeOfPayment)}>Checkout</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

