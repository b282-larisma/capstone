import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { productId } = useParams();
  const navigate = useNavigate();

  const { user } = useContext(UserContext);

  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProduct(data);
      });
  }, [productId]);

  const handleAddToCart = () => {
    if (quantity <= 0) {
      Swal.fire({
        title: 'Invalid Quantity',
        icon: 'error',
        text: 'Please enter a valid quantity greater than 0.',
      });
      return;
    }

    if (quantity > product.stocks) {
      Swal.fire({
        title: 'Insufficient Stocks',
        icon: 'error',
        text: 'The quantity you are purchasing exceeds the available stocks.',
      });
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/users/addtocart`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: 'Added to Cart',
            icon: 'success',
            text: 'The product has been added to your cart.',
          });
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.',
          });
        }
      });
  };

  const handleQuantityChange = (e) => {
    const inputQuantity = parseInt(e.target.value);
    setQuantity(inputQuantity);
  };

  return (
    <Container>
      {product && (
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card>
              <Card.Body className="text-center">
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {product.price}</Card.Text>
                <Card.Subtitle>Stocks:</Card.Subtitle>
                <Card.Text>{product.stocks}</Card.Text>

                {!user.id ? (
                  <Button as={Link} to="/login">
                    Login to Purchase
                  </Button>
                ) : (
                  <>
                    <Form.Group>
                      <Form.Label>Quantity:</Form.Label>
                      <Form.Control
                        type="number"
                        value={quantity}
                        onChange={handleQuantityChange}
                      />
                    </Form.Group>

                    <Form.Group>
                      {quantity <= 0 ? (
                        <Button variant="danger" disabled>
                          Invalid Quantity
                        </Button>
                      ) : product.stocks > 0 ? (
                        <Button onClick={handleAddToCart}>Add to Cart</Button>
                      ) : (
                        <Button variant="danger" disabled>
                          Out of Stock
                        </Button>
                      )}
                    </Form.Group>
                  </>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      )}
    </Container>
  );
}