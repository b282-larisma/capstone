import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';

function AppNavbarAdmin() {
  return (
    <Navbar expand="lg" className="bg-body-secondary">
      <Navbar.Brand as={Link} to="/admin-home">
        Department Store
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          <Nav.Link as={NavLink} to="/admin-home">
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin-all-products">
            All Products
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin-all-users">
            All Users
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin-all-orders">
            All Orders
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin-create-product">
            Create A Product
          </Nav.Link>
        </Nav>
        <Nav>
          <Nav.Link as={NavLink} to="/logout">
            Logout
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default AppNavbarAdmin;
