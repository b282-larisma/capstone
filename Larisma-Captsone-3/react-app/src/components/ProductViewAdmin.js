import { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductViewAdmin() {
  const { productId } = useParams();

  const [originalProduct, setOriginalProduct] = useState(null);
  const [editedProduct, setEditedProduct] = useState(null);
  const [hasChanges, setHasChanges] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setOriginalProduct(data);
        setEditedProduct(data);
      });
  }, [productId]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedProduct((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleUpdateProduct = (e) => {
    e.preventDefault();

    if (!hasChanges) {
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(editedProduct),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setOriginalProduct(editedProduct);
          setHasChanges(false);
          Swal.fire({
            title: 'Update Product Successful',
            icon: 'success',
            text: 'Product updated successfully.',
          });
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  useEffect(() => {
    if (
      originalProduct &&
      (originalProduct.name !== editedProduct.name ||
        originalProduct.description !== editedProduct.description ||
        originalProduct.price !== editedProduct.price ||
        originalProduct.stocks !== editedProduct.stocks)
    ) {
      setHasChanges(true);
    } else {
      setHasChanges(false);
    }
  }, [originalProduct, editedProduct]);

  return (
    <Container>
      {originalProduct && (
        <Form onSubmit={handleUpdateProduct}>
          <h1 className="text-center my-3">Edit Product</h1>

          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              name="name"
              value={editedProduct.name}
              onChange={handleInputChange}
              placeholder="Enter the product Name"
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              name="description"
              value={editedProduct.description}
              onChange={handleInputChange}
              placeholder="Enter description"
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control
              type="number"
              name="price"
              value={editedProduct.price}
              onChange={handleInputChange}
              placeholder="Enter the price"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="stocks">
            <Form.Label>Stocks</Form.Label>
            <Form.Control
              type="number"
              name="stocks"
              value={editedProduct.stocks}
              onChange={handleInputChange}
              placeholder="Enter the number of stocks"
            />
          </Form.Group>
          {hasChanges ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      )}
    </Container>
  );
}