import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function OrderCardAdmin({ order }) {
  const { _id: deliveryId, userId, product, totalAmount, modeOfPayment, isDelivered } = order;
  const [deliveryStatus, setDeliveryStatus] = useState(isDelivered);
  const [productName, setProductName] = useState('');
  const [productPrice, setProductPrice] = useState('');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${product[0].productId}`)
      .then((res) => res.json())
      .then((data) => {
        if (data.name && data.price) {
          setProductName(data.name);
          setProductPrice(data.price);
        }
      });
  }, [product]);

  const handleDelivery = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/${deliveryId}/delivered`, {
        method: 'PATCH',
      });
      const data = await response.json();
      if (data.success) {
        setDeliveryStatus(true);
        Swal.fire({
          title: 'Order Marked as Delivered',
          icon: 'success',
          text: 'The order has been marked as delivered.',
        });
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while marking the order as delivered.',
        });
      }
    } catch (error) {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'An error occurred while marking the order as delivered.',
      });
    }
  };

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>Delivery ID: {deliveryId}</h4>
            </Card.Title>
            <Card.Subtitle>User ID: {userId}</Card.Subtitle>
            <Card.Text>
              <strong>Product:</strong> {productName}
            </Card.Text>
            <Card.Text>
              <strong>Quantity:</strong> {product[0].quantity}
            </Card.Text>
            <Card.Text>
              <strong>Price:</strong> {productPrice}
            </Card.Text>
            <Card.Subtitle>Total Amount:</Card.Subtitle>
            <Card.Text>PhP {totalAmount}</Card.Text>
            <Card.Subtitle>Mode of Payment:</Card.Subtitle>
            <Card.Text>{modeOfPayment}</Card.Text>
            <Card.Subtitle>Status:</Card.Subtitle>
            <Card.Text>
              {deliveryStatus ? 'Delivered' : 'Not yet delivered'}
              {!deliveryStatus && (
                <Button className="bg-primary ms-2" onClick={handleDelivery}>
                  Mark as Delivered
                </Button>
              )}
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
