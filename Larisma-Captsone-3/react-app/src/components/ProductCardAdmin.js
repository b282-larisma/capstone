import { useState } from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductCardAdmin({ product }) {
  const { name, description, price, stocks, isActive, _id } = product;
  const [productStatus, setProductStatus] = useState(isActive);

  const handleActivate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setProductStatus(true);
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  const handleDeactivate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setProductStatus(false);
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{name}</h4>
            </Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Subtitle>Stocks</Card.Subtitle>
            <Card.Text>{stocks}</Card.Text>
            <Card.Text>{productStatus ? 'Active' : 'Not Active'}</Card.Text>
            <Button className="bg-primary" as={Link} to={`/admin-product/${_id}`}>
              Edit
            </Button>
            {productStatus ? (
              <Button className="bg-primary" onClick={handleDeactivate}>
                Deactivate
              </Button>
            ) : (
              <Button className="bg-primary" onClick={handleActivate}>
                Activate
              </Button>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}