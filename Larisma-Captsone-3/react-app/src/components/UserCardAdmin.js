import { useState } from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UserCardAdmin({ user }) {
  const { fullName, email, isAdmin, _id } = user;
  const [userStatus, setUserStatus] = useState(isAdmin);

  const handleAdminify = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/adminify`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setUserStatus(true);
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  const handleDeadminify = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${_id}/deadminify`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setUserStatus(false);
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please, try again.',
          });
        }
      });
  };

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12}>
        <Card className="cardHighlight p-0">
          <Card.Body>
            <Card.Title>
              <h4>{fullName}</h4>
            </Card.Title>
            <Card.Subtitle>Email</Card.Subtitle>
            <Card.Text>{email}</Card.Text>
            <Card.Text>{isAdmin ? 'Admin' : 'Non-Admin'}</Card.Text>
            {isAdmin ? (
              <Button className="bg-primary" onClick={handleDeadminify}>
                Make into a Non-Admin
              </Button>
            ) : (
              <Button className="bg-primary" onClick={handleAdminify}>
                Make into an Admin
              </Button>
            )}
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}