import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((fullName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [fullName, email, password1, password2])

    function registerUser(e) {      
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkfullnameandemail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fullName: fullName,
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Duplicate Full Name And Email Found",
                    icon: "error",
                    text: "Kindly provide another full name and email to complete registration."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/checkfullname`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        fullName: fullName
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if (data === true) {
                        Swal.fire({
                            title: "Duplicate Full Name Found",
                            icon: "error",
                            text: "Kindly provide another full name to complete registration."
                        })
                    } else {
                        fetch(`${process.env.REACT_APP_API_URL}/users/checkemail`, {
                            method: "POST",
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({
                                email: email
                            })
                        })
                        .then(res => res.json())
                        .then(data => {
                            if (data === true) {
                                Swal.fire({
                                    title: "Duplicate Email Found",
                                    icon: "error",
                                    text: "Kindly provide another email to complete registration."
                                })
                            } else {
                                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                                    method: "POST",
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify({
                                        fullName: fullName,
                                        email: email,
                                        password: password1
                                    })
                                })
                                .then(res => res.json())
                                .then(data => {
                                    if(data === true) {

                                        setFullName("");
                                        setEmail("");
                                        setPassword1("");
                                        setPassword2("");

                                        Swal.fire({
                                            title: "Registration Successful",
                                            icon: "success",
                                            text: "Welcome to Zuitt!"
                                        })
                                        navigate("/login");
                                    } else {
                                        Swal.fire({
                                            title: "Something went wrong",
                                            icon: "error",
                                            text: "Please, try again."
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    return (
        (user.id !== null)?
        <Navigate to ="/products" />
        :
        <Form onSubmit={(e) => registerUser(e)} >
            <h1 className="text-center my-3">Registration</h1>
            <Form.Group className="mb-3" controlId="fullName">
                <Form.Label>Full Name</Form.Label>
                <Form.Control 
                    type="text"
                    value={fullName}
                    onChange={(e) => {setFullName(e.target.value)}}
                    placeholder="Enter your Full Name" 
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email"
                    value={email}
                    onChange={(e) => {setEmail(e.target.value)}}
                    placeholder="Enter email" />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    value={password1}
                    onChange={(e) => {setPassword1(e.target.value)}}
                    placeholder="Enter Your Password" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    value={password2}
                    onChange={(e) => {setPassword2(e.target.value)}}
                    placeholder="Verify Your Password" />
            </Form.Group>
                  
            <Button
                variant="primary"
                type="submit"
                id="submitBtn"
                disabled={!isActive}
            >
                Submit
            </Button>
                  
        </Form> 
    )
}