import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import CartCard from '../components/CartCard';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';

export default function Cart() {
  const { user } = useContext(UserContext);
  const [cartItems, setCartItems] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkcart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCartItems(data);
      });
  }, []);

  const handleCheckout = (cartId, modeOfPayment) => {
    Swal.fire({
      title: 'Confirm Checkout',
      icon: 'warning',
      text: 'Are you sure you want to proceed with the checkout?',
      showCancelButton: true,
      confirmButtonText: 'Yes, checkout!',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({
            cartId: cartId,
            modeOfPayment: modeOfPayment,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            if (data.success) {
              Swal.fire({
                title: 'Order Placed!',
                icon: 'success',
                text: 'Your order has been placed successfully.',
              }).then(() => {
                if (user.id) {
                  navigate('/products');
                } else {
                  navigate('/login');
                }
              });
            } else {
              Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'An error occurred during checkout.',
              });
            }
          });
      }
    });
  };

  return (
    <Container>
      <Row>
        <Col>
          <h1 className="my-3">Shopping Cart</h1>
          {cartItems.map((item) => (
            <CartCard key={item._id} item={item} onCheckout={handleCheckout} />
          ))}
        </Col>
      </Row>
    </Container>
  );
}