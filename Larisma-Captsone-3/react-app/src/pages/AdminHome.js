import React from 'react';
import { Container, Row, Col, Table } from 'react-bootstrap';
import Chart from 'react-apexcharts';

export default function AdminHome() {
  // Sample data for the charts
  const doughnutData = {
    series: [300, 50, 100],
    options: {
      labels: ['Electronics', 'Clothing', 'Toys'],
      colors: ['#FF6384', '#36A2EB', '#FFCE56'],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: '100%',
            },
            legend: {
              position: 'bottom',
            },
          },
        },
      ],
    },
  };

  const barData = {
    series: [
      {
        name: 'Sales',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
    options: {
      chart: {
        type: 'bar',
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: false,
        },
      },
      xaxis: {
        categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      },
      colors: ['#36A2EB'],
    },
  };

  // Sample data for the pie chart
  const pieData = {
    series: [44, 55, 13, 43, 22],
    options: {
      labels: ['Apple', 'Samsung', 'Sony', 'LG', 'Nokia'],
      colors: ['#FF6384', '#36A2EB', '#FFCE56', '#FFC837', '#AD46A5'],
    },
  };

  // Sample data for the line chart
  const lineData = {
    series: [
      {
        name: 'Sales',
        data: [30, 40, 35, 50, 49, 60, 70, 91, 125],
      },
    ],
    options: {
      chart: {
        type: 'line',
        height: 350,
      },
      xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
      },
      colors: ['#FF6384'],
    },
  };

  // Sample data for the products table
  const products = [
    { id: 1, name: 'Smartphone', category: 'Electronics', quantity: 150, price: 500 },
    { id: 2, name: 'Laptop', category: 'Electronics', quantity: 100, price: 1000 },
    { id: 3, name: 'T-Shirt', category: 'Clothing', quantity: 200, price: 20 },
    { id: 4, name: 'Jeans', category: 'Clothing', quantity: 120, price: 50 },
    { id: 5, name: 'Action Figure', category: 'Toys', quantity: 300, price: 15 },
    { id: 6, name: 'Board Game', category: 'Toys', quantity: 80, price: 30 },
  ];

  return (
    <Container>
      <Row className="mt-5">
        <Col>
          <h1>Welcome, Admin!</h1>
          <p>This is the admin dashboard. You can view charts, tables, and manage orders and products here.</p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col xs={12} md={6}>
          <h2 className="mb-3">Most Purchased Product for the Month</h2>
          <Chart options={doughnutData.options} series={doughnutData.series} type="donut" width="100%" />
        </Col>
        <Col xs={12} md={6}>
          <h2 className="mb-3">Sales Trend</h2>
          <Chart options={barData.options} series={barData.series} type="bar" height={350} />
        </Col>
      </Row>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <h2 className="mb-3">Top Product Brands</h2>
          <Chart options={pieData.options} series={pieData.series} type="pie" width="100%" />
        </Col>
        <Col xs={12} md={6}>
          <h2 className="mb-3">Sales Performance</h2>
          <Chart options={lineData.options} series={lineData.series} type="line" height={350} />
        </Col>
      </Row>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={8}>
          <h2 className="mb-3">Products Inventory</h2>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>ID</th>
                <th>Product Name</th>
                <th>Category</th>
                <th>Quantity</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product.id}>
                  <td>{product.id}</td>
                  <td>{product.name}</td>
                  <td>{product.category}</td>
                  <td>{product.quantity}</td>
                  <td>${product.price}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
}
