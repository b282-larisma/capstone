import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminCreateProduct() {

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if(name !== "" && description !== "" && price !== 0 && stocks !== 0) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price, stocks])

    function createProduct(e) {      
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/checkproductname`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: 'Duplicate Product Name Found',
                    icon: 'error',
                    text: 'Please provide another product name.'
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                        stocks: stocks
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data === true) {

                        setName('');
                        setDescription('');
                        setPrice(0);
                        setStocks(0);

                        Swal.fire({
                            title: 'Create Product Successful',
                            icon: 'success',
                            text: 'Product created successfully.',
                        })
                    } else {
                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })
    }

    return (
        <Form onSubmit={(e) => createProduct(e)}>
            <h1 className="text-center my-3">Create Product</h1>

            <Form.Group className="mb-3" controlId="name">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    placeholder="Enter the product name"
                    required
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="text"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    placeholder="Enter description"
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="number"
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}
                    placeholder="Enter the price"
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="stocks">
                <Form.Label>Stocks</Form.Label>
                <Form.Control
                    type="number"
                    value={stocks}
                    onChange={(e) => setStocks(e.target.value)}
                    placeholder="Enter the number of stocks"
                />
            </Form.Group>

            {isActive ? (
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
            ) : (
                <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            )}
        </Form>
    );
}