import UserCardAdmin from '../components/UserCardAdmin';
import { useState, useEffect } from 'react';

export default function AdminAllUsers() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/all`)
      .then((res) => res.json())
      .then((data) => {
        setUsers(data);
      });
  }, []);

  return (
    <>
      {users.map((user) => (
        <UserCardAdmin key={user._id} user={user} />
      ))}
    </>
  );
}
