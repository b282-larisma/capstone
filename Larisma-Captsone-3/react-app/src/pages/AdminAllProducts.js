import ProductCardAdmin from '../components/ProductCardAdmin';
import { useState, useEffect } from 'react';

export default function AdminAllProducts() {
  const [products, setProducts] = useState([]);

useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/products/all`)
    .then(res => res.json())
    .then(data => {
      setProducts(data);
    })
}, []);

  return (
    <>
      {products.map(product => (
        <ProductCardAdmin key={product._id} product={product} />
      ))}
    </>
  );
}