import React, {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import OrderCardAdmin from '../components/OrderCardAdmin';

export default function AdminAllOrders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetchOrdersData();
  }, []);

  const fetchOrdersData = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/orders/all`);
      const data = await response.json();
      setOrders(data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Container>
      <h1 className="my-3">All Orders</h1>
      {orders.map((order) => (
        <OrderCardAdmin key={order._id} order={order} />
      ))}
    </Container>
  );
}
