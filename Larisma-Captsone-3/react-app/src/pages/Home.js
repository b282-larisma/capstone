import { useContext, useEffect, useState } from 'react';
import { Container, Row, Col, Card, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Banner from '../components/Banner';

export default function Home() {
  	const { user } = useContext(UserContext);
  	const [fullName, setFullName] = useState('');

  	useEffect(() => {
    	fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      		headers: {
        		Authorization: `Bearer ${localStorage.getItem('token')}`,
      		},
    	})
      	.then((res) => res.json())
      	.then((data) => {
        	if (data.fullName) {
          		setFullName(data.fullName);
        	}
      	});
  	}, []);

  	const topProducts = [
    	{ name: 'Fashionable Dress', price: 100, imageUrl: 'http://1.bp.blogspot.com/-pRpK_EmX27k/UoEAlT0_YrI/AAAAAAAA7_k/YPlcVOv4X6o/s1600/elbise1.jpg' },
    	{ name: 'Elegant Watch', price: 200, imageUrl: 'https://i.pinimg.com/originals/d0/d7/35/d0d73521e54fba8f7c239ced70629d32.jpg' },
    	{ name: 'Stylish Handbag', price: 300, imageUrl: 'https://fiver.media/images/mu/2016/07/27/stripe-square-handbag-38051-6.jpg' },
  	];

  	const promotions = [
    	{ name: 'Summer Sale', description: 'Special offer on Fashionable Dress', imageUrl: 'https://cdnc.lystit.com/photos/814c-2014/04/29/cutie-blue-multicoloured-squares-print-dress-product-1-19622819-2-981330688-normal.jpeg' },
    	{ name: 'Watch Clearance', description: 'Limited-time discount on Elegant Watch', imageUrl: 'https://www.christies.com/img/LotImages/2013/GNV/2013_GNV_01398_0207_000(rolex_a_rare_and_attractive_platinum_and_diamond-set_automatic_calenda053846).jpg' },
  	];

  	const randomProducts = [
    	{ name: 'Sporty Sneakers', price: 150, imageUrl: 'https://guhaha.com/wp-content/uploads/2020/06/H7877ff1250ff4393a3b7b6c42d70a153B-1.jpg' },
    	{ name: 'Trendy Sunglasses', price: 250, imageUrl: 'https://ae01.alicdn.com/kf/HTB1Kw4Jwb1YBuNjSszhq6AUsFXaR/YOOSKE-2018-Oversized-Sunglasses-Women-Elegant-Frameless-Glitter-Color-Sun-glasses-Ladies-Shades-Vintage-Big-Frame.jpg' },
    	{ name: 'Luxury Perfume', price: 350, imageUrl: 'https://colorsglass.com/Uploads/products/2020-03-11/en-50ml-Square-Perfume-Bottle-Parfum-Bottle-Luxury.jpg' },
  	];

 	const bannerData = {
    	title: user.id ? `Welcome back, ${fullName}!` : 'Welcome to Department Store',
    	content: 'Necessities for everyone, everywhere.',
    	destination: '/products',
    	label: 'Shop now!',
  	};

	return (
	    <>
	      	<Banner data={bannerData} />
	      	<Container className="mt-4">
	        	<Row>
	          		<Col>
	            		<h2>Top Products</h2>
	            		<Row>
	              			{topProducts.map((product) => (
		                		<Col key={product.name}>
		                  			<Card>
			                    		<Card.Img variant="top" src={product.imageUrl} />
			                    		<Card.Body>
			                      		<Card.Title>{product.name}</Card.Title>
			                      		<Link to="/products" className="btn btn-primary">
			                        		View Product
			                      		</Link>
			                    		</Card.Body>
		                  			</Card>
		                		</Col>
	              			))}
	            		</Row>
	          		</Col>
	        	</Row>

	        	{user.id && (
	          		<Row className="mt-4">
	            		<Col>
	              			<h2>Hot Products</h2>
	              			<Carousel className="col-8">
	                			{randomProducts.map((product) => (
		                  			<Carousel.Item key={product.name}>
			                    		<img className="d-block w-100" src={product.imageUrl} alt={product.name} />
			                    		<Carousel.Caption>
			                      			<h3>{product.name}</h3>
			                    		</Carousel.Caption>
		                  			</Carousel.Item>
	                			))}
	             			 </Carousel>
	            		</Col>
	         		 </Row>
	        	)}

	        	<Row className="mt-4">
	          		<Col>
	            		<h2>Promotions</h2>
	            		<Row>
	              			{promotions.map((promotion) => (
	                			<Col key={promotion.name}>
		                  			<Card>
			                    		<Card.Img variant="top" src={promotion.imageUrl} />
			                    		<Card.Body>
				                      		<Card.Title>{promotion.name}</Card.Title>
				                      		<Link to="/promotions" className="btn btn-primary">
				                        		View Promotion
				                      		</Link>
			                    		</Card.Body>
		                  			</Card>
	                			</Col>
	              			))}
	            		</Row>
	          		</Col>
	        	</Row>
	      	</Container>
	    </>
	);
}
