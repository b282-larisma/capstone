import React, { useEffect, useState, useContext } from 'react';
import { Card, Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Profile() {
  const { user } = useContext(UserContext);
  const [fullName, setFullName] = useState('');
  const [cash, setCash] = useState(0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.fullName && data.cash) {
          setFullName(data.fullName);
          setCash(data.cash);
        }
      });
  }, []);

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Profile Details</Card.Title>
              <Card.Text>
                <strong>Full Name:</strong> {fullName}
              </Card.Text>
              <Card.Text>
                <strong>Cash:</strong> PhP {cash}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
