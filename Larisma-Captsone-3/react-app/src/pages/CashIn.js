import { useState, useEffect } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function CashIn() {
  const [cardNumber, setCardNumber] = useState('');
  const [pin, setPin] = useState('');
  const [amount, setAmount] = useState('');
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (cardNumber !== '' && pin !== '' && !isNaN(Number(amount)) && Number(amount) > 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [cardNumber, pin, amount]);

  const handleSubmit = (e) => {
    e.preventDefault();

    const cashInAmount = Number(amount);
    const requestBody = {
      cardNumber,
      pin,
      cash: cashInAmount,
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/cashin`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(requestBody),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data)
        if (data === true) {
          Swal.fire({
            title: 'Cash In Successful',
            icon: 'success',
            text: 'You have successfully added cash to your account.',
          })
            navigate('/products');
        } else {
          Swal.fire({
            title: 'Cash In Error',
            icon: 'error',
            text: 'An error occurred during the cash-in process. Please try again.',
          });
        }
      });
  };

  return (
    <Card className="my-3">
      <Card.Header>
        <h4>Cash In</h4>
      </Card.Header>
      <Card.Body>
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="cardNumber">
            <Form.Label>Credit Card / Bank Card Number</Form.Label>
            <Form.Control
              type="text"
              value={cardNumber}
              onChange={(e) => setCardNumber(e.target.value)}
              maxLength={16}
              placeholder="Enter card number"
              required
            />
          </Form.Group>
          <Form.Group controlId="pin">
            <Form.Label>PIN</Form.Label>
            <Form.Control
              type="password"
              value={pin}
              onChange={(e) => setPin(e.target.value)}
              maxLength={6}
              placeholder="Enter PIN"
              required
            />
          </Form.Group>
          <Form.Group controlId="amount">
            <Form.Label>Amount to Cash In</Form.Label>
            <Form.Control
              type="text" 
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              placeholder="Enter amount"
              require
            />
          </Form.Group>
          <Button variant="primary" type="submit" disabled={!isActive}>
            Cash In
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
}
