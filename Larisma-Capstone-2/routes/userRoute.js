const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Retrieving all the users
router.get("/all", (req, res) => {
    userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

// Check if the email and fullname exist
router.post("/checkfullnameandemail", (req, res) => {
	userController.checkFullNameAndEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// Check if the full name exists
router.post("/checkfullname", (req, res) => {
	userController.checkFullNameExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Check if the email exists
router.post("/checkemail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// User registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Retrieve user details
router.get("/profile", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Cashing in
router.patch("/cashin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.cashIn({userId: userData.id}, req.body).then(resultFromController => res.send(resultFromController))
});

// Adminify a user
router.patch("/:userId/adminify", (req, res) => {
    userController.adminifyUser(req.params).then(resultFromController => res.send(resultFromController))
});

// De-adminify a user
router.patch("/:userId/deadminify", (req, res) => {
    userController.deadminifyUser(req.params).then(resultFromController => res.send(resultFromController))
});

// Adding a product to cart
router.post("/addtocart", (req, res) => {

	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	};

	userController.addToCart(req.params, data).then(resultFromController => res.send(resultFromController))
});

// Checking the cart
router.get("/checkcart", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.checkCart({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Checkout an order
router.post("/checkout", auth.verify, (req, res) => {

  const data = {
    userId: auth.decode(req.headers.authorization).id,
    modeOfPayment: req.body.modeOfPayment,
    cartId: req.body.cartId
  };

  userController.checkout(req.params, data)
    .then(resultFromController => res.send(resultFromController));
});

module.exports = router