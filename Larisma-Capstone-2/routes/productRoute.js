const express = require("express")
const router = express.Router()
const productController = require("../controllers/productController")
const auth = require("../auth")

// Check if the product name exists
router.post("/checkproductname", (req, res) => {
    productController.checkProductNameExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Creating a product
router.post("/create", (req, res) => {
    productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieving all the products
router.get("/all", (req, res) => {
    productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

// Retrieving active products
router.get("/active", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Retrieving a specific product
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Updating a product
router.put("/:productId/update", (req, res) => {
    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Archiving a product
router.patch("/:productId/archive", (req, res) => {
    productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Activate a product
router.patch("/:productId/activate", (req, res) => {
    productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router