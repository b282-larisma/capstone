const express = require("express")
const router = express.Router()
const orderController = require("../controllers/orderController")
const auth = require("../auth")

// Retrieving all orders
router.get("/all", (req, res) => {
    orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
})

// Add a new route to mark an order as delivered
router.patch("/:orderId/delivered", (req, res) => {
  orderController.markOrderAsDelivered(req.params.orderId).then((result) => {
    if (result) {
      res.send({ success: true });
    } else {
      res.send({ success: false });
    }
  });
});

module.exports = router