const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required!"]
	},
	description: {
		type: String,
		required: [true, "Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrder: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema)