const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  	userId: String,
  	product: [
    	{
      		productId: String,
      		quantity: Number,
    	}
  	],
  	totalAmount: Number,
  	modeOfPayment: {
    	type: String,
    	required: [true, "Mode of payment is required"],
  	},
  	purchasedOn: {
    	type: Date,
    	default: new Date(),
  	},
  	isDelivered: {
    	type: Boolean,
    	default: false,
  	},
});

module.exports = mongoose.model('Order', orderSchema);
