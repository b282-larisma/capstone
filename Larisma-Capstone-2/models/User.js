const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	fullName : {
		type : String,
		required : [true, "Full name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	cash : {
		type : Number,
		default : 0
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	cart : [
		{
			product : [
				{
					productId : {
									type : String,
									required : [true, "Product ID is required"]
								},
					productName : {
									type : String,
									required : [true, "Product name is required"]
								},
					quantity : {
									type : Number,
									required : [true, "Quantity is required"]
								}
				}
			],
			total : Number,
		}
	]
})

module.exports = mongoose.model("User", userSchema);