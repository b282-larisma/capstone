const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const userRoute = require("./routes/userRoute")
const productRoute = require("./routes/productRoute")
const orderRoute = require("./routes/orderRoute")
const app = express()

const port = 4000

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://benedictlarisma2001:jpjEXz5875h5hm41@wdc028-course-booking.bz7joyt.mongodb.net/productPurchasingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"))

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())

app.use("/users", userRoute)
app.use("/products", productRoute)
app.use("/orders", orderRoute)

app.listen(port, () => console.log(`Server running at port ${port}!`))