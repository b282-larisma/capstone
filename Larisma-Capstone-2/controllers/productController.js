const Product = require("../models/Product")
const auth = require("../auth")

// Check if product name exists
module.exports.checkProductNameExists = (reqBody) => {
    return Product.find({name : reqBody.name}).then(res => {
        if (res.length > 0) {
            return true;
        } else {
            return false;
        };
    });
};

// Creating a new product
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stocks: reqBody.stocks
    })

    return newProduct.save().then((product, err) => {
        if (err) {
            return false
        } else {
            return true
        }
    })
}

// Retrieving all the products
module.exports.getAllProduct = () => {
    return Product.find().then(res => {
        if (!res) {
            return false
        } else {
            return res
        }
    })
}

// Retrieving active products
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then(res => {
        if (!res) {
            return false
        } else {
            return res
        }
    })  
}

// Retrieving specific product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(res => {
        if (!res) {
            return false
        } else {
            return res
        }
    })
}

// Updating a product
module.exports.updateProduct = (reqParams, reqBody) => {
    return Product.findById(reqParams.productId).then((res, err) => {

        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            stocks: reqBody.stocks
        }

        if (
            res.name === reqBody.name &&
            res.description === reqBody.description &&
            res.price === reqBody.price &&
            res.stocks === reqBody.stocks
        ) {
            return false;
        } else {
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err) => {
                if (err) {
                    return false;
                } else {
                    return true;
                }
            });
        }
    });
};

// Archiving a product
module.exports.archiveProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then((res, err) => {
        if(err){
            return false
        } else if (res.isActive) {
            res.isActive = false
            return res.save().then((archiveProduct, saveErr) => {
                if(saveErr){
                    return false
                } else {
                    return true
                }
            }) 
        } else if (!res.isActive) {
            return false
        }
    })
}

// Activating a product
module.exports.activateProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then((res, err) => {
        if(err){
            return false
        } else if (!res.isActive) {
            res.isActive = true
            return res.save().then((archiveProduct, saveErr) => {
                if(saveErr){
                    return false
                } else {
                    return true
                }
            }) 
        } else if (res.isActive) {
            return false
        }
    })
}