const User = require("../models/User")
const Order = require("../models/Order")
const auth = require("../auth")

// Retrieving all orders
module.exports.getAllOrders = () => {
    return Order.find({}).then(res => {
        if (!res || res.length === 0) {
            return false
        } else {
            return res
        }
    })
}

// Mark an order as delivered
module.exports.markOrderAsDelivered = (orderId) => {
  return Order.findByIdAndUpdate(
    orderId,
    { $set: { isDelivered: true } },
    { new: true }
  ).then((updatedOrder) => {
    if (!updatedOrder) {
      return false;
    } else {
      return true;
    }
  });
};