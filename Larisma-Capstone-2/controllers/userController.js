const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require("bcrypt")
const auth = require("../auth")

// Retrieving all the users
module.exports.getAllUsers = () => {
    return User.find().then(res => {
        if (!res) {
            return false
        } else {
            return res
        }
    })
}

// Check if email and full name exists
module.exports.checkFullNameAndEmailExist = (reqBody) => {
    return User.find({fullName : reqBody.fullName}).then(res => {
        if (res.length > 0) {
            return User.find({email : reqBody.email}).then(res => {
                if (res.length > 0) {
                    return true;
                } else {
                    return false;
                };
            });
        } else {
            return false;
        };
    });
};

// Check if full name exists
module.exports.checkFullNameExists = (reqBody) => {
    return User.find({fullName : reqBody.fullName}).then(res => {
        if (res.length > 0) {
            return true;
        } else {
            return false;
        };
    });
};

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(res => {
        if (res.length > 0) {
            return true;
        } else {
            return false;
        };
    });
};

// User registration
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        fullName : reqBody.fullName,
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, err) => {
        if(err) {
            return err
        } else {
            return true
        }
    })
}

// User authentication
module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(res => {
        if(res == null){
            return false
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, res.password);
            if (isPasswordCorrect) {
                return { access : auth.createAccessToken(res) }
            } else {
                return false
            }
        }
    })
}

// Retrieve user details
module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(res => {
        res.password = ""
        return res
    })
}

// Cash in
module.exports.cashIn = (data, reqBody) => {
    return User.findById(data.userId).then((res, err) => {
        if(err){
            return false
        } else if (!res) {
            return false
        }

        let totalCash = res.cash + reqBody.cash
        res.cash = totalCash

        return res.save().then((cashIn, saveErr) => {
            if(saveErr){
                return false
            } else {
                return true
            }
        })
    })
}

// Adminify a user
module.exports.adminifyUser = (reqParams) => {
    return User.findById(reqParams.userId).then((res, err) => {
        if(err){
            return false
        } 

        res.isAdmin = true

        return res.save().then((adminifyUser, saveErr) => {
            if(saveErr){
                return false
            } else {
                return true
            }
        })
    })
}

// De-adminify a user
module.exports.deadminifyUser = (reqParams) => {
    return User.findById(reqParams.userId).then((res, err) => {
        if(err){
            return false
        } 

        res.isAdmin = false

        return res.save().then((deadminifyUser, saveErr) => {
            if(saveErr){
                return false
            } else {
                return true
            }
        })
    })
}

// Add a product to cart
module.exports.addToCart = (reqParams, data) => {
  return User.findById(data.userId).then(user => {
      return Product.findById(data.productId).then(product => {
          if (!product) {
            return false
          }

            let amount = product.price * data.quantity
            let name = product.name

            user.cart.push({
                product: {
              productId: data.productId,
              productName: name,
              quantity: data.quantity
            },
            total: amount
          })

            return user.save().then((addToCart, saveErr) => {
                if (saveErr) {
                    return false
                } else {
                    return true
                }
            })
        })
    })
}

// Check cart
module.exports.checkCart = (data) => {
  return User.findById(data.userId)
    .then((res) => {
      return res.cart;
    });
};

// Checkout an order
module.exports.checkout = async (reqParams, data) => {
  try {
    const user = await User.findById(data.userId);
    const cartIndex = user.cart.findIndex((item) => item._id.toString() === data.cartId);

    if (cartIndex === -1) {
      return { success: false, message: 'Cart not found' };
    }

    const orderData = user.cart[cartIndex];
    const orderProductId = orderData.product[0].productId;
    const newCash = user.cash - orderData.total;
    user.cash = newCash;
    const isUserUpdated = await user.save();

    const isProductUpdated = await Product.findById(orderProductId).then((product) => {
      product.userOrder.push({ userId: data.userId });
      return product.save().then((product, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    });

    if (isUserUpdated && isProductUpdated) {
      
      const orderProductName = orderData.product[0].productName;
      const orderProductQuantity = orderData.product[0].quantity;
      const newStocks = orderProductQuantity - 1;
      const isStockUpdated = await Product.updateOne({ _id: orderProductId }, { $set: { stocks: newStocks } });

      if (!isStockUpdated) {
        return { success: false, message: 'Failed to update product stocks' };
      }

      const orderProductTotal = orderData.total;
      const newOrder = new Order({
        userId: data.userId,
        product: {
          productId: orderProductId,
          productName: orderProductName,
          quantity: orderProductQuantity,
        },
        totalAmount: orderProductTotal,
        modeOfPayment: data.modeOfPayment,
      });

      await newOrder.save();
      user.cart.splice(cartIndex, 1);
      await user.save();
      return { success: true, message: 'Order placed successfully' };
    } else {
      return { success: false, message: 'An error occurred during checkout' };
    }
  } catch (error) {
    return { success: false, message: 'An error occurred during checkout' };
  }
};